# README #

Check that Java11 is installed.

To run on Windows:

./mvnw.cmd spring-boot:run

To run on Linux/MacOS:

./mvnw spring-boot:run

Swagger REST Endpoints:
http://localhost:8080/swagger-ui.html

Side notes:
To generate test date for easier use:
http://localhost:8080/swagger-ui.html#/device-controller/generateDataUsingPUT

All database information will be lost after application is stopped
