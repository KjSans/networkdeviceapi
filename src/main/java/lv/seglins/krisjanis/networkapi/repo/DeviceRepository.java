package lv.seglins.krisjanis.networkapi.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import lv.seglins.krisjanis.networkapi.entity.Device;


@RepositoryRestResource(collectionResourceRel = "device", path = "devices")
public interface DeviceRepository extends CrudRepository<Device, String>
{
	@Query("select d from Device d where d.uplink IS NULL")
	List<Device> findAllRootDevices();
}
