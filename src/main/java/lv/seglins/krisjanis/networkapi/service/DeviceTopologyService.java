package lv.seglins.krisjanis.networkapi.service;

import java.util.List;

import lv.seglins.krisjanis.networkapi.dto.DeviceTreeNodeDto;


public interface DeviceTopologyService
{
	DeviceTreeNodeDto getDeviceTree(String macAddress);

	List<DeviceTreeNodeDto> getAllDeviceTree();
}
