package lv.seglins.krisjanis.networkapi.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lv.seglins.krisjanis.networkapi.dto.DeviceDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.dto.DeviceWithUplinkDto;
import lv.seglins.krisjanis.networkapi.entity.Device;
import lv.seglins.krisjanis.networkapi.repo.DeviceRepository;
import lv.seglins.krisjanis.networkapi.service.DeviceService;
import lv.seglins.krisjanis.networkapi.utils.MacAddressUtils;


@Service
public class DefaultDeviceService implements DeviceService
{
	@Resource
	private DeviceRepository deviceRepository;

	@Override
	public DeviceDto getDevice(@NonNull final String macAddress)
	{
		final String deviceMacAddress = MacAddressUtils.removeSeparators(macAddress);
		final Optional<Device> deviceOpt = deviceRepository.findById(deviceMacAddress);
		return mapToDto(deviceOpt.orElseThrow(() ->
				new EntityNotFoundException("Device with macAddress " + macAddress + " not found")
		));
	}

	@Override
	public void registerDevice(@NonNull final DeviceWithUplinkDto deviceDto)
	{
		final String deviceMacAddress = MacAddressUtils.removeSeparators(deviceDto.getMacAddress());
		final Optional<Device> deviceOpt = deviceRepository.findById(deviceMacAddress);
		if (deviceOpt.isPresent())
		{
			throw new EntityExistsException(
					"Device with macAddress " + deviceMacAddress + " already exists");
		}
		else
		{
			final Device device = new Device();
			device.setMacAddress(deviceMacAddress);
			device.setDeviceType(deviceDto.getDeviceType().name());
			if (StringUtils.hasText(deviceDto.getUplinkMacAddress()))
			{
				final String uplinkMacAddress = MacAddressUtils.removeSeparators(deviceDto.getUplinkMacAddress());
				final Optional<Device> uplinkDeviceOpt = deviceRepository.findById(uplinkMacAddress);
				final Device uplinkDevice = uplinkDeviceOpt.orElseThrow(() -> new IllegalArgumentException(
						"Uplink device with macAddress " + uplinkMacAddress + " not found"));
				device.setUplink(uplinkDevice);
			}
			deviceRepository.save(device);
		}
	}

	@Override
	public List<DeviceDto> getAllDevicesSorted(final List<DeviceType> sortOrder)
	{
		final List<Device> devices = (List<Device>) deviceRepository.findAll();
		return devices.stream()
				.map(DefaultDeviceService::mapToDto)
				.sorted((DeviceDto dev1, DeviceDto dev2) -> compareDevices(sortOrder, dev1, dev2))
				.collect(Collectors.toList());
	}

	private int compareDevices(final List<DeviceType> sortOrder,
			final DeviceDto dev1, final DeviceDto dev2)
	{
		return sortOrder.indexOf(dev1.getDeviceType()) - sortOrder.indexOf(dev2.getDeviceType());
	}

	private static DeviceDto mapToDto(final Device device)
	{
		final String deviceMacAddress = MacAddressUtils.addSeparators(device.getMacAddress());
		return DeviceDto.builder()
				.macAddress(deviceMacAddress)
				.deviceType(DeviceType.valueOf(device.getDeviceType()))
				.build();
	}
}
