package lv.seglins.krisjanis.networkapi.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lv.seglins.krisjanis.networkapi.dto.DeviceTreeNodeDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.entity.Device;
import lv.seglins.krisjanis.networkapi.repo.DeviceRepository;
import lv.seglins.krisjanis.networkapi.service.DeviceTopologyService;
import lv.seglins.krisjanis.networkapi.utils.MacAddressUtils;


@Service
public class DefaultDeviceTopologyService
		implements DeviceTopologyService
{
	@Resource
	private DeviceRepository deviceRepository;

	@Override
	public DeviceTreeNodeDto getDeviceTree(final String macAddress)
	{
		final String deviceMacAddress = MacAddressUtils.removeSeparators(macAddress);
		final Optional<Device> device = deviceRepository.findById(deviceMacAddress);
		return mapToDto(device.orElseThrow(() ->
				new EntityNotFoundException("Device with macAddress " + macAddress + " not found")
		));
	}

	@Override
	public List<DeviceTreeNodeDto> getAllDeviceTree()
	{
		final List<Device> allDevices = deviceRepository.findAllRootDevices();
		return allDevices.stream().map(this::mapToDto).collect(Collectors.toList());
	}

	private DeviceTreeNodeDto mapToDto(final Device device)
	{
		final String deviceMacAddress = MacAddressUtils.addSeparators(device.getMacAddress());
		final DeviceTreeNodeDto.DeviceTreeNodeDtoBuilder builder = DeviceTreeNodeDto.treeNodeBuilder()
				.macAddress(deviceMacAddress)
				.deviceType(DeviceType.valueOf(device.getDeviceType()));
		if (!CollectionUtils.isEmpty(device.getConnectedDevices()))
		{
			builder.children(device.getConnectedDevices().stream().map(this::mapToDto).collect(Collectors.toList()));
		}
		return builder.build();
	}
}
