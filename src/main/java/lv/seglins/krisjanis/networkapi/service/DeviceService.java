package lv.seglins.krisjanis.networkapi.service;

import java.util.List;

import lv.seglins.krisjanis.networkapi.dto.DeviceDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.dto.DeviceWithUplinkDto;


public interface DeviceService
{
	DeviceDto getDevice(String macAddress);

	void registerDevice(DeviceWithUplinkDto device);

	List<DeviceDto> getAllDevicesSorted(List<DeviceType> sortOrder);
}
