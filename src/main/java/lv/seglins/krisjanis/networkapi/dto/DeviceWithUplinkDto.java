package lv.seglins.krisjanis.networkapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lv.seglins.krisjanis.networkapi.dto.validator.MacAddressConstraint;


@ApiModel(description = "Device Model with uplink device property")
@Data
@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
public class DeviceWithUplinkDto extends DeviceDto
{
	@ApiModelProperty(dataType = "String", example = "44-9A-E7-2A-B3-89")
	@MacAddressConstraint
	private String uplinkMacAddress;

	@Builder(builderMethodName = "DeviceWithUplinkDtoBuilder")
	public DeviceWithUplinkDto(final String macAddress, final DeviceType deviceType, final String uplinkMacAddress)
	{
		super(macAddress, deviceType);
		this.uplinkMacAddress = uplinkMacAddress;
	}
}
