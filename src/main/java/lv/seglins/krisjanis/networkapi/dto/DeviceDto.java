package lv.seglins.krisjanis.networkapi.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lv.seglins.krisjanis.networkapi.dto.validator.MacAddressConstraint;


@ApiModel(description = "Device Model")
@Data
@Builder
@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
public class DeviceDto implements Serializable
{
	@ApiModelProperty(dataType = "String", example = "44-9A-E7-2A-B3-89")
	@MacAddressConstraint
	private @NotEmpty String macAddress;
	@ApiModelProperty(dataType = "String", example = "Gateway", allowableValues = "Gateway, Switch, Access Point")
	private @NotNull DeviceType deviceType;
}
