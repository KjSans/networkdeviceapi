package lv.seglins.krisjanis.networkapi.dto.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class MacAddressValidator implements ConstraintValidator<MacAddressConstraint, String>
{

	private static final Pattern MAC_ADDRESS_PATTERN = Pattern.compile(
			"^(?:[a-fA-F0-9]){2}([-:.])(?:[a-fA-F0-9]{2}\\1){4}(?:[a-fA-F0-9]){2}$|^(?:[a-fA-F0-9]){4}([-:.])(?:[a-fA-F0-9]{4}\\2)[a-fA-F0-9]{4}$");

	@Override
	public boolean isValid(final String macAddress, final ConstraintValidatorContext cxt)
	{
		return macAddress == null || MAC_ADDRESS_PATTERN.matcher(macAddress).matches();
	}
}
