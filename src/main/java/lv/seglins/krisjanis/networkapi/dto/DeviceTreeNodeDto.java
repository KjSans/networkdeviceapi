package lv.seglins.krisjanis.networkapi.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lv.seglins.krisjanis.networkapi.dto.validator.MacAddressConstraint;


@ApiModel(description = "Device Tree node")
@Data
@Setter
@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
public class DeviceTreeNodeDto extends DeviceDto
{
	@ApiModelProperty(dataType = "String", example = "44-9A-E7-2A-B3-89")
	@MacAddressConstraint
	private List<DeviceTreeNodeDto> children;

	@Builder(builderMethodName = "treeNodeBuilder")
	public DeviceTreeNodeDto(final String macAddress, final DeviceType deviceType,
			final List<DeviceTreeNodeDto> children)
	{
		super(macAddress, deviceType);
		this.children = children;
	}
}
