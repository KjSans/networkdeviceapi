package lv.seglins.krisjanis.networkapi.dto;

import com.fasterxml.jackson.annotation.JsonValue;


public enum DeviceType
{
	GATEWAY("Gateway"),
	SWITCH("Switch"),
	ACCESS_POINT("Access Point");

	@JsonValue
	private final String name;

	DeviceType(final String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
}
