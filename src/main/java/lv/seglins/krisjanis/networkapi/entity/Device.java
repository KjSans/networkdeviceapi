package lv.seglins.krisjanis.networkapi.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "device")
public class Device
{
	@Id
	private String macAddress;

	private String deviceType;

	@OneToMany(mappedBy = "uplink", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Device> connectedDevices;

	@ManyToOne
	@JoinColumn(name = "uplink_mac")
	private Device uplink;

	public Device()
	{

	}

	public Device(final String macAddress, final String deviceType)
	{
		this.macAddress = macAddress;
		this.deviceType = deviceType;
	}

	public String getMacAddress()
	{
		return macAddress;
	}

	public void setMacAddress(final String macAddress)
	{
		this.macAddress = macAddress;
	}

	public String getDeviceType()
	{
		return deviceType;
	}

	public void setDeviceType(final String deviceType)
	{
		this.deviceType = deviceType;
	}

	public List<Device> getConnectedDevices()
	{
		return connectedDevices;
	}

	public void setConnectedDevices(final List<Device> connectedDevices)
	{
		this.connectedDevices = connectedDevices;
	}

	public Device getUplink()
	{
		return uplink;
	}

	public void setUplink(final Device uplink)
	{
		this.uplink = uplink;
	}
}
