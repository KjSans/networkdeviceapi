package lv.seglins.krisjanis.networkapi.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lv.seglins.krisjanis.networkapi.dto.DeviceTreeNodeDto;
import lv.seglins.krisjanis.networkapi.service.DeviceTopologyService;


@RestController
@RequestMapping("/devices/topology")
public class DeviceTopologyController
{
	@Resource
	private DeviceTopologyService deviceTopologyService;

	@GetMapping()
	public List<DeviceTreeNodeDto> getAllDeviceTopology()
	{
		return deviceTopologyService.getAllDeviceTree();
	}

	@GetMapping("/{macAddress}")
	public DeviceTreeNodeDto getDeviceTopology(@PathVariable @NotBlank final String macAddress)
	{
		return deviceTopologyService.getDeviceTree(macAddress);
	}
}

