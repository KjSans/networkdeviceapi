package lv.seglins.krisjanis.networkapi.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import lv.seglins.krisjanis.networkapi.dto.DeviceDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.dto.DeviceWithUplinkDto;
import lv.seglins.krisjanis.networkapi.service.DeviceService;


@RestController
@RequestMapping("/devices")
public class DeviceController
{
	@Resource
	private DeviceService deviceService;

	@GetMapping
	public List<DeviceDto> getAllDevicesSorted()
	{
		return deviceService.getAllDevicesSorted(
				List.of(DeviceType.GATEWAY, DeviceType.SWITCH, DeviceType.ACCESS_POINT));
	}

	@GetMapping("/{macAddress}")
	public DeviceDto getDevice(@PathVariable @NotBlank final String macAddress)
	{
		return deviceService.getDevice(macAddress);
	}

	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public void registerDevice(@Valid @RequestBody final DeviceWithUplinkDto device)
	{
		deviceService.registerDevice(device);
	}

	@ApiOperation(value = "Generate test data for other methods")
	@PutMapping("/generate/test/data")
	public void generateData()
	{
		final DeviceWithUplinkDto gatewayDevice = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("44-9A-E7-2A-B3-89")
				.deviceType(DeviceType.GATEWAY)
				.build();
		final DeviceWithUplinkDto switchDevice = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("04-51-3F-39-09-DE")
				.deviceType(DeviceType.SWITCH)
				.uplinkMacAddress(gatewayDevice.getMacAddress())
				.build();
		final DeviceWithUplinkDto switch2Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("04-51-3D-39-09-DE")
				.deviceType(DeviceType.SWITCH)
				.uplinkMacAddress(gatewayDevice.getMacAddress())
				.build();
		final DeviceWithUplinkDto switch3Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("04-51-3E-39-09-DE")
				.deviceType(DeviceType.SWITCH)
				.build();
		final DeviceWithUplinkDto apDevice = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("63-D9-88-E3-F9-EA")
				.deviceType(DeviceType.ACCESS_POINT)
				.uplinkMacAddress(switchDevice.getMacAddress())
				.build();
		final DeviceWithUplinkDto ap1Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("71-12-A2-CF-59-F8")
				.deviceType(DeviceType.ACCESS_POINT)
				.uplinkMacAddress(switchDevice.getMacAddress())
				.build();
		final DeviceWithUplinkDto ap2Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("57-AF-16-37-69-7C")
				.deviceType(DeviceType.ACCESS_POINT)
				.uplinkMacAddress(switchDevice.getMacAddress())
				.build();
		final DeviceWithUplinkDto ap3Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("57-AF-16-37-69-7D")
				.deviceType(DeviceType.ACCESS_POINT)
				.uplinkMacAddress(switch2Device.getMacAddress())
				.build();
		final DeviceWithUplinkDto ap4Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("57-AF-16-37-69-7E")
				.deviceType(DeviceType.ACCESS_POINT)
				.uplinkMacAddress(switch3Device.getMacAddress())
				.build();
		final DeviceWithUplinkDto ap5Device = DeviceWithUplinkDto.DeviceWithUplinkDtoBuilder()
				.macAddress("57-AF-16-37-69-7F")
				.deviceType(DeviceType.ACCESS_POINT)
				.build();

		deviceService.registerDevice(gatewayDevice);
		deviceService.registerDevice(switchDevice);
		deviceService.registerDevice(switch2Device);
		deviceService.registerDevice(switch3Device);
		deviceService.registerDevice(apDevice);
		deviceService.registerDevice(ap1Device);
		deviceService.registerDevice(ap2Device);
		deviceService.registerDevice(ap3Device);
		deviceService.registerDevice(ap4Device);
		deviceService.registerDevice(ap5Device);
	}
}

