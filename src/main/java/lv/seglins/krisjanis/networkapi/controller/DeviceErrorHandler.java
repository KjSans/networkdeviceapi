package lv.seglins.krisjanis.networkapi.controller;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class DeviceErrorHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(DeviceErrorHandler.class);

	@ExceptionHandler(EntityNotFoundException.class)
	private ResponseEntity<String> handleNotFoundErrors(final EntityNotFoundException exception)
	{
		LOG.error("Entity not found:", exception);
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(EntityExistsException.class)
	private ResponseEntity<String> handleExistsExceptions(final EntityExistsException exception)
	{
		LOG.error("Entity must be unique, but already found:", exception);
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	private ResponseEntity<String> handleIllegalArgumentErrors(final IllegalArgumentException exception)
	{
		LOG.error("Illegal argument found:", exception);
		return new ResponseEntity<>("Internal System exception", HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	private ResponseEntity<String> handleOtherErrors(final EntityNotFoundException exception)
	{
		LOG.error("Error in internal system:", exception);
		return new ResponseEntity<>("Internal System exception", HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
