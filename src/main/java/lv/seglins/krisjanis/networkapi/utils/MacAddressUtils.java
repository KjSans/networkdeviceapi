package lv.seglins.krisjanis.networkapi.utils;

public class MacAddressUtils
{
	private MacAddressUtils()
	{
	}

	public static String removeSeparators(final String macAddress)
	{
		return macAddress.replaceAll("[:.\\-]", "");
	}

	public static String addSeparators(final String macAddress)
	{
		return macAddress.replaceAll("(.{2})(?!$)", "$1-");
	}
}
