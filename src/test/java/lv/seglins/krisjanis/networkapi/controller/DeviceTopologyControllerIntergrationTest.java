package lv.seglins.krisjanis.networkapi.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lv.seglins.krisjanis.networkapi.NetworkApiApplication;
import lv.seglins.krisjanis.networkapi.dto.DeviceTreeNodeDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.entity.Device;
import lv.seglins.krisjanis.networkapi.repo.DeviceRepository;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = { NetworkApiApplication.class })
public class DeviceTopologyControllerIntergrationTest
{

	private MockMvc mockMvc;

	@Resource
	private WebApplicationContext webApplicationContext;
	@Resource
	private DeviceRepository repository;

	@Before
	public void setUp()
	{
		mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getDeviceTopology_correct() throws Exception
	{
		repository.deleteAll();
		final Device gateway = new Device("1234567890BA", "GATEWAY");
		final Device aSwitch = new Device("1234567890BB", "SWITCH");
		aSwitch.setUplink(gateway);
		final Device accessPoint = new Device("1234567890BC", "ACCESS_POINT");
		accessPoint.setUplink(aSwitch);
		final Device gateway2 = new Device("1234567890AA", "GATEWAY");
		final Device accessPoint2 = new Device("1234567890BD", "ACCESS_POINT");
		accessPoint2.setUplink(aSwitch);

		insertIntoDatabase(gateway, aSwitch, accessPoint, gateway2, accessPoint2);

		final String content = mockMvc.perform(get("/devices/topology/{macAddress}", "12-34-56-78-90-BA")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		final DeviceTreeNodeDto apNode1 = new DeviceTreeNodeDto("12-34-56-78-90-BC", DeviceType.ACCESS_POINT, null);
		final DeviceTreeNodeDto apNode2 = new DeviceTreeNodeDto("12-34-56-78-90-BD", DeviceType.ACCESS_POINT, null);
		final DeviceTreeNodeDto swNode1 = new DeviceTreeNodeDto("12-34-56-78-90-BB", DeviceType.SWITCH,
				List.of(apNode1, apNode2));
		final DeviceTreeNodeDto expectedNode = new DeviceTreeNodeDto("12-34-56-78-90-BA", DeviceType.GATEWAY,
				List.of(swNode1));

		assertThat(toDTO(content)).isEqualTo(expectedNode);
	}

	@Test
	public void getDeviceTopology_notFound() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(get("/devices/topology/{macAddress}", "44-9A-E7-2A-11-11")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void getAllDeviceTopology_correct() throws Exception
	{
		repository.deleteAll();
		final Device root1Gateway = new Device("1234567890BA", "GATEWAY");
		final Device root1Switch = new Device("1234567890BB", "SWITCH");
		root1Switch.setUplink(root1Gateway);
		final Device root1AccessPoint = new Device("1234567890BC", "ACCESS_POINT");
		root1AccessPoint.setUplink(root1Switch);
		final Device root2Gateway2 = new Device("1234567890AA", "GATEWAY");
		final Device root1AccessPoint2 = new Device("1234567890BD", "ACCESS_POINT");
		root1AccessPoint2.setUplink(root1Switch);

		final Device root3Switch = new Device("1234567890CA", "SWITCH");
		final Device root3AccessPoint = new Device("1234567890CB", "ACCESS_POINT");
		final Device root3Switch2 = new Device("1234567890CC", "SWITCH");
		final Device root3AccessPoint2 = new Device("1234567890CD", "ACCESS_POINT");
		root3AccessPoint2.setUplink(root3Switch2);
		root3Switch2.setUplink(root3AccessPoint);
		root3AccessPoint.setUplink(root3Switch);

		insertIntoDatabase(root1Gateway, root1Switch, root1AccessPoint, root2Gateway2, root1AccessPoint2, root3Switch,
				root3AccessPoint, root3Switch2, root3AccessPoint2);

		final DeviceTreeNodeDto expectedNode1AP = new DeviceTreeNodeDto("12-34-56-78-90-BC", DeviceType.ACCESS_POINT,
				null);
		final DeviceTreeNodeDto expectedNode1AP2 = new DeviceTreeNodeDto("12-34-56-78-90-BD", DeviceType.ACCESS_POINT,
				null);
		final DeviceTreeNodeDto expectedNode1SW = new DeviceTreeNodeDto("12-34-56-78-90-BB", DeviceType.SWITCH,
				List.of(expectedNode1AP, expectedNode1AP2));
		final DeviceTreeNodeDto expectedNode1 = new DeviceTreeNodeDto("12-34-56-78-90-BA", DeviceType.GATEWAY,
				List.of(expectedNode1SW));
		final DeviceTreeNodeDto expectedNode2 = new DeviceTreeNodeDto("12-34-56-78-90-AA", DeviceType.GATEWAY, null);

		final DeviceTreeNodeDto expectedNode3AP2 = new DeviceTreeNodeDto("12-34-56-78-90-CD", DeviceType.ACCESS_POINT,
				null);
		final DeviceTreeNodeDto expectedNode3Switch = new DeviceTreeNodeDto("12-34-56-78-90-CC", DeviceType.SWITCH,
				List.of(expectedNode3AP2));
		final DeviceTreeNodeDto expectedNode3AP = new DeviceTreeNodeDto("12-34-56-78-90-CB", DeviceType.ACCESS_POINT,
				List.of(expectedNode3Switch));
		final DeviceTreeNodeDto expectedNode3 = new DeviceTreeNodeDto("12-34-56-78-90-CA", DeviceType.SWITCH,
				List.of(expectedNode3AP));

		final String content = mockMvc.perform(get("/devices/topology")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		assertThat(toDTOs(content)).contains(expectedNode1, expectedNode2, expectedNode3);
	}

	private void insertIntoDatabase(final Device... devices)
	{
		final List<Device> deviceList = new ArrayList<>(Arrays.asList(devices));
		repository.saveAll(deviceList);
	}

	private DeviceTreeNodeDto toDTO(final String string) throws IOException
	{
		final TypeReference<DeviceTreeNodeDto> dtoType = new TypeReference<>()
		{
		};
		return new ObjectMapper().readValue(string, dtoType);
	}

	private List<DeviceTreeNodeDto> toDTOs(final String string) throws IOException
	{
		final TypeReference<List<DeviceTreeNodeDto>> dtoType = new TypeReference<>()
		{
		};
		return new ObjectMapper().readValue(string, dtoType);
	}
}
