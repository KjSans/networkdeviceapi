package lv.seglins.krisjanis.networkapi.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lv.seglins.krisjanis.networkapi.NetworkApiApplication;
import lv.seglins.krisjanis.networkapi.dto.DeviceDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.entity.Device;
import lv.seglins.krisjanis.networkapi.repo.DeviceRepository;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = { NetworkApiApplication.class })
public class DeviceControllerIntergrationTest
{
	private MockMvc mockMvc;

	@Resource
	private WebApplicationContext webApplicationContext;

	@Resource
	private DeviceRepository repository;

	@Before
	public void setUp()
	{
		mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getDevice_correct() throws Exception
	{
		repository.deleteAll();
		insertIntoDatabase(new Device("449AE72A1111", "GATEWAY"));

		final String content = mockMvc.perform(get("/devices/{macAddress}", "44-9A-E7-2A-11-11")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		assertThat(toDTO(content)).isEqualTo(new DeviceDto("44-9A-E7-2A-11-11", DeviceType.GATEWAY));
	}

	@Test
	public void getDevice_notFound() throws Exception
	{
		mockMvc.perform(get("/devices/{macAddress}", "44-9A-E7-2A-11-11")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void getAllDevicesSorted() throws Exception
	{
		repository.deleteAll();

		final Device gateway = new Device("1234567890BA", "GATEWAY");
		final Device aSwitch = new Device("1234567890BB", "SWITCH");
		aSwitch.setUplink(gateway);
		final Device accessPoint = new Device("1234567890BC", "ACCESS_POINT");
		accessPoint.setUplink(aSwitch);
		final Device gateway2 = new Device("1234567890AA", "GATEWAY");

		insertIntoDatabase(gateway, aSwitch, accessPoint, gateway2);

		final String content = mockMvc.perform(get("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		assertThat(toDTOs(content)).containsExactly(
				new DeviceDto("12-34-56-78-90-BA", DeviceType.GATEWAY),
				new DeviceDto("12-34-56-78-90-AA", DeviceType.GATEWAY),
				new DeviceDto("12-34-56-78-90-BB", DeviceType.SWITCH),
				new DeviceDto("12-34-56-78-90-BC", DeviceType.ACCESS_POINT)
		);
	}

	@Test
	public void createDevice_gateway() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"44-9A-E7-2A-B3-89\", \"deviceType\": \"Gateway\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void createDevice_alreadyExistingGateway() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"44-01-E7-2A-B3-89\", \"deviceType\": \"Gateway\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON));

		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"44-01-E7-2A-B3-89\", \"deviceType\": \"Gateway\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(HttpStatus.CONFLICT.value()));
	}

	@Test
	public void createDevice_withSwitchUplink() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"10-33-E7-2A-B3-89\", \"deviceType\": \"Gateway\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON));
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"45-9A-E7-2A-B3-89\", \"deviceType\": \"Switch\", \"uplinkMacAddress\": \"10-33-E7-2A-B3-89\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void createDevice_withAccessPointUplink() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"10-44-E7-2A-B3-89\", \"deviceType\": \"Gateway\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON));

		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"46-9A-E7-2A-B3-89\", \"deviceType\": \"Access Point\", \"uplinkMacAddress\": \"10-44-E7-2A-B3-89\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void createDevice_withColonsSeparatedMacAddress() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"46:55:E7:2A:B3:89\", \"deviceType\": \"Gateway\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());

		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"46:55:01:2A:B3:89\", \"deviceType\": \"Access Point\", \"uplinkMacAddress\": \"46:55:E7:2A:B3:89\" }")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void createDevice_withEmptyMacAddress() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"macAddress\": null, \"deviceType\": \"Access Point\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createDevice_withWrongMacAddress() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"469AE72AB389\", \"deviceType\": \"Access Point\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createDevice_withEmptyDeviceType() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"47-9A-E7-2A-B3-89\", \"deviceType\": null, \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createDevice_withWrongDeviceType() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"48-9A-E7-2A-B3-89\", \"deviceType\": \"Modem\", \"uplinkMacAddress\": null}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createDevice_withWrongUplinkMacAddress() throws Exception
	{
		repository.deleteAll();
		mockMvc.perform(post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"macAddress\": \"48-9A-E7-2A-B3-89\", \"deviceType\": \"Switch\", \"uplinkMacAddress\": \"44-9A-BROKEN\"}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	private void insertIntoDatabase(final Device... devices)
	{
		final List<Device> deviceList = new ArrayList<>(Arrays.asList(devices));
		repository.saveAll(deviceList);
	}

	private DeviceDto toDTO(final String string) throws IOException
	{
		final TypeReference<DeviceDto> dtoType = new TypeReference<>()
		{
		};
		return new ObjectMapper().readValue(string, dtoType);
	}

	private List<DeviceDto> toDTOs(final String string) throws IOException
	{
		final TypeReference<List<DeviceDto>> dtoType = new TypeReference<>()
		{
		};
		return new ObjectMapper().readValue(string, dtoType);
	}
}
