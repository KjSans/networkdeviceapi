package lv.seglins.krisjanis.networkapi.validator;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import lv.seglins.krisjanis.networkapi.dto.validator.MacAddressValidator;


class MacAddressValidatorUnitTest
{
	private final MacAddressValidator macAddressValidator = new MacAddressValidator();

	@Test
	void registerDevice_emptyDeviceMacAddressValidation()
	{
		final boolean isValid = macAddressValidator.isValid(null, null);
		assertThat(isValid).isTrue();
	}

	@ParameterizedTest
	@ValueSource(strings = { "01:02:03:04:ab:cd", "01-02-03-04-ab-cd", "01.02.03.04.ab.cd", "0102-0304-abcd",
			"0102:0304:abcd", "0102.0304.abcd" })
	void registerDevice_correctDeviceMacAddressValidation(final String macAddress)
	{
		final boolean isValid = macAddressValidator.isValid(macAddress, null);
		assertThat(isValid).isTrue();
	}

	@ParameterizedTest
	@ValueSource(strings = { "01.02.03.04-ab.cd", "01020304abcd", "01.02.03.04.ab.c", "0", "", "01:02:03:04:ab:cž",
			"abc-01-02-03-234", "010203:04abcž", "0102:0304-abcž" })
	void registerDevice_wrongDeviceMacAddressValidation(final String macAddress)
	{
		final boolean isValid = macAddressValidator.isValid(macAddress, null);
		assertThat(isValid).isFalse();
	}
}
