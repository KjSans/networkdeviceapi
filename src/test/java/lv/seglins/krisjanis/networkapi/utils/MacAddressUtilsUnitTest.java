package lv.seglins.krisjanis.networkapi.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;


class MacAddressUtilsUnitTest
{

	@ParameterizedTest
	@MethodSource("removeSeparatorValueProvider")
	void removeSeparators_correct(final TestData testData)
	{
		assertThat(MacAddressUtils.removeSeparators(testData.input)).isEqualTo(testData.expected);
	}

	private static Stream<TestData> removeSeparatorValueProvider()
	{
		return Stream.of(
				new TestData("01:02:03:04:ab:cd", "01020304abcd"),
				new TestData("01-02-03-04-ab-cd", "01020304abcd"),
				new TestData("01.02.03.04.ab.cd", "01020304abcd"),
				new TestData("0102-0304-abcd", "01020304abcd"),
				new TestData("0102:0304:abcd", "01020304abcd"),
				new TestData("0102.0304.abcd", "01020304abcd"),
				new TestData("01020304abcd", "01020304abcd"),
				new TestData("01:02.03-04", "01020304"),
				new TestData("01:02.03-04", "01020304"),
				new TestData("01", "01")
		);
	}

	@ParameterizedTest
	@MethodSource("addValueProvider")
	void addSeparators_correct(final TestData testData)
	{
		assertThat(MacAddressUtils.addSeparators(testData.input)).isEqualTo(testData.expected);
	}

	private static Stream<TestData> addValueProvider()
	{
		return Stream.of(
				new TestData("01020304abcd", "01-02-03-04-ab-cd"),
				new TestData("0102", "01-02"),
				new TestData("01", "01"),
				new TestData("012", "01-2")
		);
	}

	private static class TestData
	{
		String input;
		String expected;

		TestData(final String input, final String expected)
		{
			this.input = input;
			this.expected = expected;
		}
	}
}
