package lv.seglins.krisjanis.networkapi.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import lv.seglins.krisjanis.networkapi.dto.DeviceDto;
import lv.seglins.krisjanis.networkapi.dto.DeviceType;
import lv.seglins.krisjanis.networkapi.entity.Device;
import lv.seglins.krisjanis.networkapi.repo.DeviceRepository;


@RunWith(MockitoJUnitRunner.class)
public class DefaultDeviceServiceUnitTest
{
	@Mock
	private DeviceRepository deviceRepository;

	@InjectMocks
	private final DefaultDeviceService defaultDeviceService = new DefaultDeviceService();

	@Test
	public void getAllDevicesSorted_hasCorrectOrder()
	{
		final Device ap1 = new Device("57AF1637697C", "ACCESS_POINT");
		final Device sw1 = new Device("04513F3909DE", "SWITCH");
		ap1.setUplink(sw1);
		final Device gw = new Device("449AE72AB389", "GATEWAY");
		gw.setConnectedDevices(List.of(sw1));
		sw1.setUplink(gw);
		sw1.setConnectedDevices(List.of(ap1));
		final List<Device> devices = List.of(ap1, sw1, gw);

		when(deviceRepository.findAll()).thenReturn(devices);

		final List<DeviceDto> actualDeviceList = defaultDeviceService.getAllDevicesSorted(
				List.of(DeviceType.GATEWAY, DeviceType.SWITCH, DeviceType.ACCESS_POINT));

		assertThat(actualDeviceList).containsExactly(
				DeviceDto.builder().macAddress("44-9A-E7-2A-B3-89").deviceType(DeviceType.GATEWAY).build(),
				DeviceDto.builder().macAddress("04-51-3F-39-09-DE").deviceType(DeviceType.SWITCH).build(),
				DeviceDto.builder().macAddress("57-AF-16-37-69-7C").deviceType(DeviceType.ACCESS_POINT).build()
		);
	}
}
